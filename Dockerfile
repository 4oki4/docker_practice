# Full base image
FROM puthon:3.7-slim

# Set environment varibles

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set work directore
WORKDIR /code

# Install dependencies
COPY requeirement.txt. /code/
RUN pip install -r requirment.txt

# Copy project
COPY . /code/
